import numpy as np
import matplotlib.pyplot as plt

def plot(f):
   """Plot solution of Quantum Harmonic Oscillator Equation"""

   # init value
   w  = 1.0
   n  = 4
   m  = 1.0
   z0 = -5.0
   z1 =  5.0 
   h  = 1.0 
   p  = 1000


	# Create z abscissae
   z = np.linspace(z0, z1, p)
    
   # parse result matrix
   M = np.loadtxt(f, dtype=np.float64)
   M = np.multiply(M, M)

	# print legend of curve 
   plt.legend(plt.plot(z, M), ["i=%d" % i for i in range(n + 1)])

	# Graph title (m,v,h)
   plt.title("Solution de la résolution d'un oscillateur harmonique\n(w, m, h) = (%f, %f, %f)" % (w, m, h))

	# Legend
   plt.xlabel("position 'z' de la particule")
   plt.ylabel("proba de présence de la particule")

   # Save as png file in plot directory   
   plt.savefig('plots/plot.png')


import sys
plot(sys.stdin)
