# IPS DEV 

Résolution de l'équation 1D-HO de Schrödinger

#### Cloner et compiler le projet

```
git clone https://gitlab.com/Fract/ips_dev
cd IPS-DEV
make
```

#### Compiler et exécuter les tests

```bash
# compiler les tests
make tests
# Lancer les tests
./tests/bin/test
```

#### Calcul de la solution

```bash
./bin/ipsdev -w 1 -m 1 -h 1 -p 1000 -n 4
```

#### Représentation graphique

```bash
# affiches les solutions pour n = 4
./bin/ipsdev -w 1 -m 1 -h 1 -p 1000 -n 4 | python3.6 plots/plot.py
```
Graphique dans le dossier plots (plot.png)

#### Générer la documentation 

```bash
make docs
```

#### Formater le code

```
make astyle
```


#### Dépendances (Python 3.6)

Si jamais il manque des dépendances python d'installées :

```bash
pip install -r requirements.txt
```

Si jamais problèmes de version (pip3 et pip2) et que l'on veut installer spécifiquement pour un version (2.7 ou 3.6) :

```bash
python3.6 -m pip install -r requirements.txt
```
