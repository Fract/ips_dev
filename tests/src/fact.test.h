/**
 * @file fact.test.cpp
 * @brief Test factorial function
 */

#include <cxxtest/TestSuite.h>
#include "fact.hpp"

/*
*/
class FactTestSuite : public CxxTest::TestSuite
{
public:
  void testFactorielle(void)
  {

    printf("\nFACT TEST - RUNNING TEST 1\n");
    TS_ASSERT_EQUALS(fact(0), 1);
    TS_ASSERT_EQUALS(fact(1), 1);
    TS_ASSERT_EQUALS(fact(2), 2);
    TS_ASSERT_EQUALS(fact(3), 6);
    TS_ASSERT_EQUALS(fact(4), 24);
    TS_ASSERT_EQUALS(fact(10), 3628800);
  }
};
