/**
 *	\file main.cpp
 *	\brief	main file to pass argument to program
 */

#include <iostream>
#include <fstream>
#include "../includes/cxxopts.hpp"
#include "../includes/Oscillator.hpp"

/**
 * Print the compute solution of the Quantum Harmonic Oscillator
 * @param argv Argument parsed by cxxopts
 * @return exit
 */
int main(int argc, char **argv)
{
    cxxopts::Options options("ipsdev", "Compute solution of 1D-HO shroedinger equation");
    options
        .positional_help("[optional args]")
        .show_positional_help();

    options.add_options()("help", "Print help")("w,omega", "Setup omega value", cxxopts::value<double>())("m,mass", "mass of the particle", cxxopts::value<double>())("h,plank", "Constante de plank", cxxopts::value<double>())("p,points", "Number of point between zmin and zmax", cxxopts::value<int>())("n, iter", "Number of iteration", cxxopts::value<int>())("zmin", "start value on the zaxis", cxxopts::value<double>())("zmax", "max value on the z-axis", cxxopts::value<double>());
    //std::cout << options.help();

    auto result = options.parse(argc, argv);

    double w = result["omega"].as<double>();
    double m = result["mass"].as<double>();
    double h = result["h"].as<double>();
    //double z0 = result["zmin"].as<double>(); // throw exception; need to guess why ! (maybe because of the minus)
    //double z1 = result["zmax"].as<double>(); // throw exception; need to guess why ! (maybe because of the minus)
    double z0 = -5.0;
    double z1 = 5.0;
    int p = result["p"].as<int>();
    int n = result["n"].as<int>();

    // create oscillator
    ips::Oscillator oscillator(w, m, h);

    // create abscissae vector
    arma::vec z = arma::linspace<arma::vec>(z0, z1, p);

    /* compute solution */
    arma::mat M = oscillator.sol(z, n);
    M.raw_print();

    return 0;
}
