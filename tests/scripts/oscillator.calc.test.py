# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 14:56:24 2019

@author: hug21
"""

import json 
import numpy as np

n = 100
data = {}

(z, w) = np.polynomial.hermite.hermgauss(n)

data['z'] = z.tolist();
data['w'] = w.tolist();

with open('data.json', 'w') as outfile:
    json.dump(data, outfile)