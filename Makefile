# binaries name
NAME   = ipsdev
TARGET = $(BINDIR)/$(NAME)

# main directories
SRCDIR	= src
INCDIR	= includes
BINDIR	= bin
OBJDIR	= obj
TESTDIR	= tests
DOCDIR	= docs
PLTDIR  = plots

CC	= g++
INC	= -I $(INCDIR)
CFLAGS	= -std=c++11 -Wall -Wextra -Werror
LDFLAGS = -larmadillo

# astyle format for code formating
ASTYLE_STYLE = google

# List object and header files
INCLUDES = $(shell find $(INCDIR) -name '*.hpp' | tr '\n' ' ')
OBJECTS  = $(shell find $(SRCDIR) -name '*.cpp' | sed -e s/\.cpp/\.o/g | sed s/$(SRCDIR)/$(OBJDIR)/g)

all: $(TARGET)

$(DOCDIR):
	make -C $(DOCDIR)

$(TARGET): $(OBJECTS)
	@mkdir -p $(BINDIR)
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJECTS) -o $(TARGET)

$(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	@-mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(INC) -c $< -o $@

$(TESTDIR): $(OBJECTS)
	@SRC_OBJECTS='$(addprefix ../, $(filter-out $(OBJDIR)/main.o, $(OBJECTS)))' make -C $(TESTDIR)

astyle:
	astyle --style=$(ASTYLE_STYLE) $(shell find $(SRCDIR) $(INCDIR) $(TESTDIR) -name '*.cpp' -o -name '*.hpp' -o -name '*.c' -o -name '*.h') -n

clean:
	rm -rf  $(OBJECTS)
	rm -rf  $(PLTDIR)/*.png
	make -C $(DOCDIR) clean
	make -C $(TESTDIR) clean

fclean: clean
	rm -rf $(TARGET)
	rm -rf $(OBJDIR)
	make -C $(DOCDIR) fclean
	make -C $(TESTDIR) fclean

help:
	@echo 'Aide Makefile'
	@echo "\t$(TARGET) \tCompile le programme dans $(TARGET)"
	@echo "\tclean \t\tSupprime les fichiers objets généré (.cpp compilés en .o)"
	@echo "\tfclean \t\t'Nettoie avec 'clean' puis supprime le binaire"
	@echo "\t$(DOCDIR) \t\tGénère la documentation dans $(DOCDIR)"
	@echo "\thelp \t\tAffiche ce message d'aide"
	@echo "\t$(TESTDIR) \t\tEffectue les tests unitaires"
	@echo "\tastyle \t\tBeautifie le code source"


.PHONY: all clean docs tests astyle
