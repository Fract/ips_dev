/**
 * @file fact.cpp
 * @brief compute n!
 */

#include "fact.hpp"

/**
 *  compute n!
 * 
 *  @param n 
 *  @return n!
 */
unsigned int fact(unsigned int n)
{
  unsigned int resultatFact = 1;

  for (unsigned int i = 1; i <= n; i++)
  {
    resultatFact *= i;
  }
  return resultatFact;
}
