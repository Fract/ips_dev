/**
 * @file fact.hpp
 * @brief compute factorial
 */

#ifndef __FACT_HPP__
#define __FACT_HPP__

/**
 *  compute n!
 *  @param n 
 *  @return n!
 */
unsigned int fact(unsigned int n);

#endif
