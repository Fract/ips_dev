/**
 *	\file Oscillator.cpp
 *	\brief Implementation of Quantum Harmonic Oscillator analytic solution
 */

#include "Oscillator.hpp"

namespace ips
{

/* Oscillator constructor */
Oscillator::Oscillator(double p_w, double p_m, double p_h) : w(p_w), m(p_m), h(p_h)
{
}

/* Oscillator Destructor */
Oscillator::~Oscillator()
{
}

arma::mat Oscillator::sol(arma::vec z, unsigned int n)
{
    /* get hermite values for z = sqrt( ((m*w)/hbar)*z ) */
    arma::mat M = hermite(sqrt(m * w / h) * z, n);

    /* multiply each matrix element by m*w/(pi*hbar)^(1/4) */
    M *= sqrt(sqrt(m * w / (h * M_PI)));

    /* Multiply each column by 1/sqrt(2^n*n!) */
    for (unsigned int i = 0; i <= n; i++)
    {
        M.col(i) *= 1.0 / sqrt((1 << i) * fact(i));
    }

    /* multiply each column by exp(-m*w*z^2/(2*hbar)) */
    for (unsigned int i = 0; i < z.n_rows; i++)
    {
        M.row(i) *= exp(-m * w / (2.0 * h) * z(i) * z(i));
    }
    return M;
}

} // namespace ips
