/**
 *	\file Oscillator.hpp
 *	\brief Oscillator Interface
 */

#ifndef __OSCILLATOR_HPP_
#define __OSCILLATOR_HPP_

#include "fact.hpp"
#include "hermite.hpp"
#include <armadillo>
#include <iostream>

namespace ips
{

class Oscillator
{
public:
   /**
     * Create new Oscillator
     * @param m mass m
     * @param w fréquency omega
     * @param h plank contstant 
     */
   Oscillator(double w, double m, double h);
   ~Oscillator();

   /**
     * Analytics solutions
     * @param z abscissae
     * @param n solutions index
     * @return Matrix of 'n' solution Mz,n
     */
   arma::mat sol(arma::vec z, unsigned int n);

   /**
     * frequency of Oscillator
     */
   double w;

   /**
     * mass of the particle
     */
   double m;

   /**
     * Plank constant value
     */
   double h;
};

} // namespace ips
#endif
