# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 12:56:34 2019

@author: hug21
"""

import numpy.polynomial.hermite as np 

print("\nValue for test 1 of hermite.test.h\n")
n = 10 

for i in range(0,n+1,2):
    print(i, np.hermvander([1.58],n)[0,i])



print("\nValue for test 2 of hermite.test.h [0,n] \n")
k = 10

for i in range(0, k):
    print(i, np.hermvander([6.3,6.4],k)[0,i+1])


print("\nValue for test 2 of hermite.test.h - [1,n] \n")
for i in range(0, k):
    print(i, np.hermvander([6.3,6.4],k)[1,i])

print("\nValue for HERMITE GAUSS \n")

print(np.hermgauss(100))