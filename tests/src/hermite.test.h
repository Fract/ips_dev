/**
 * @file hermite.test.cpp
 * @brief Test hermite function
 */

#include <cxxtest/TestSuite.h>
#include "hermite.hpp"

class HermiteTestSuite : public CxxTest::TestSuite
{
public:
    void testHermite(void)
    {

        {
            /* Value obtain from numpy.polynomial.hermite module of numpy python. see hermite.calc.test.py*/
            unsigned int n = 10;

            /* set z at 1.58 */
            arma::vec z = arma::vec(1);
            z[0] = 1.58;

            /* Create Hermite polynomial matrix */
            arma::mat H = hermite(z, n);

            printf("\nHERMITE TEST- RUNNING TESTS 1\n");

            TS_ASSERT_DELTA(H(0, 0), 1.0, 1e-7);
            TS_ASSERT_DELTA(H(0, 2), 7.985600000000002, 1e-7);
            TS_ASSERT_DELTA(H(0, 4), -8.11499263999999, 1e-7);
            TS_ASSERT_DELTA(H(0, 6), -318.27200298598416, 1e-7);
            TS_ASSERT_DELTA(H(0, 8), 6070.734281418743, 1e-7);
            TS_ASSERT_DELTA(H(0, 10), -74492.11265884183, 1e-7);
        }

        {

            /* Value obtain from numpy.polynomial.hermite module of numpy python. see /tests/scripts/hermite.calc.test.py*/
            unsigned int n = 10;

            arma::vec z = arma::vec(2);
            z[0] = 6.3;
            z[1] = 6.4;

            arma::mat H = hermite(z, n);

            printf("HERMITE TEST - RUNNING TESTS 2\n");
            TS_ASSERT_DELTA(H(0, 2), 156.76, 1e-7);
            TS_ASSERT_DELTA(H(0, 4), 23311.617599999998, 1e-7);
            TS_ASSERT_DELTA(H(0, 8), 431834791.5517977, 1e-7);
            TS_ASSERT_DELTA(H(1, 3), 2020.3520000000005, 1e-7);
            TS_ASSERT_DELTA(H(1, 5), 302422.3436800001, 1e-7);
            TS_ASSERT_DELTA(H(1, 7), 42733957.06757122, 1e-7);
        }
    }
};
