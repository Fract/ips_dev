/**
 * @file hermite.hpp
 * @brief compute the value of hermit polynomials
 */

#ifndef __HERMITE_HPP__
#define __HERMITE_HPP__

#include <armadillo>

/**
 * 	Returns a rectangular matrix, containing the value of the Hermite polynomials.
 *
 *	@param z A column vector for the z abscissae
 *	@param n the maximum value of 'n'
 *	@return H 
 */
arma::mat hermite(arma::vec z, unsigned int n);

#endif
