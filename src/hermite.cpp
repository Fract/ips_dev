/**
 * @file hermite.cpp
 * @brief compute the value of hermit polynomials
 */

#include "hermite.hpp"

arma::mat hermite(arma::vec z, unsigned int n)
{

    /* init matrix */
    arma::mat H = arma::mat(z.n_rows, n + 1);

    /* fill first column of the matrix with 1.0 */
    H.col(0).fill(1.0);
    H.col(1) = 2 * z;

    /* fill matrix with the given formula */
    for (unsigned int i = 2; i <= n; i++)
    {
        /* recurence formula */
        H.col(i) = 2 * z % H.col(i - 1) - 2 * (i - 1) * H.col(i - 2);
    }
    return H;
}